FROM ubuntu:16.04

RUN apt-get update && apt-get install -y --force-yes python-pip build-essential libssl-dev libffi-dev python-dev supervisor \
	libmysqlclient-dev  && mkdir /apps/

COPY requirements.txt /apps/

RUN pip install --upgrade pip && pip install -r /apps/requirements.txt


COPY app/ /apps/

COPY supervisor.conf /etc/supervisor/conf.d/

ARG MYSQL_USER='root'
ARG MYSQL_PASS='strongpass'
ARG MYSQL_HOST='127.0.0.1'
ARG MYSQL_PORT='3306'
ARG MYSQL_DB='flasky'
ARG APP_PORT='5050'

ENV MYSQL_USER=$MYSQL_USER MYSQL_PASS=$MYSQL_PASS MYSQL_HOST=$MYSQL_HOST MYSQL_PORT=$MYSQL_PORT MYSQL_DB=$MYSQL_DB APP_PORT=$APP_PORT

WORKDIR /apps/

RUN touch flask.log

COPY flasky_2017-07-17.sql /apps/

CMD supervisord