import tornado.ioloop
import tornado.web
import os
import sys

static_path = os.path.join(os.getcwd(), 'static')

settings = {'debug': True}


handlers = [(r'/static/(.*)', tornado.web.StaticFileHandler, {'path': static_path})]
try:
    app = tornado.web.Application(handlers=handlers, debug=True)
    app.listen(9000)
    tornado.ioloop.IOLoop.instance().start()
except KeyboardInterrupt:
    sys.exit(1)