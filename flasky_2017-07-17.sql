# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.43-0ubuntu0.14.04.1-log)
# Database: flasky
# Generation Time: 2017-07-17 05:53:39 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table customer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(80) DEFAULT NULL,
  `last_name` varchar(80) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `ccn` varchar(80) DEFAULT NULL,
  `username` varchar(80) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;

INSERT INTO `customer` (`id`, `first_name`, `last_name`, `email`, `ccn`, `username`, `password`)
VALUES
	(1,'Mathew','Gordon','tclements@yahoo.com','4815672447262','Mathew.Gordon','9a00f2375035f975cd0e24a1712c4de4'),
	(2,'Jackie','Cox','laura86@miller.net','4660637139996','Jackie.Cox','eba4d8b2eb58e7f39399edb98c3f2f8d'),
	(3,'Laurie','Williamson','casecrystal@shepard-singleton.org','3337178082170962','Laurie.Williamson','54cabc096f3f3d4611ea00080a229c4c'),
	(4,'Rachel','Jenkins','thomas81@lamb.com','6011128917920731','Rachel.Jenkins','90e245fa4da02d8c645ac9ec086e24fc'),
	(5,'Joseph','Chavez','matthewsavage@hotmail.com','340454777858794','Joseph.Chavez','9dfd98fc01451cddad350211f424d29f'),
	(6,'Keith','Day','pmclaughlin@green-cohen.com','3112176803442612','Keith.Day','8c68d7814b0a7507ef8988e4f948be8b'),
	(7,'Bradley','Burns','hunter20@daniels-gill.org','4428453959357986','Bradley.Burns','5965ecfe63989fa1e7f3aa5c336c4213'),
	(8,'Stephanie','Lester','danny48@yahoo.com','4732095253195','Stephanie.Lester','1f721e66ba7bad9630eb4ace69905e23'),
	(9,'Tina','Dunn','andersoncharles@yahoo.com','3158221844664512','Tina.Dunn','b18a276330954f94623ad17f1aae1651'),
	(10,'Carolyn','White','ecampos@garcia-juarez.info','180079765293911','Carolyn.White','e25d61294899cd24d1156be822a79734'),
	(11,'Rebecca','Christensen','suzanne19@hotmail.com','630497127658','Rebecca.Christensen','412bd3634e90527c81adf3358494857a'),
	(12,'Raymond','Carlson','lauriegamble@williams.com','4571646240982','Raymond.Carlson','916df24c344ec030d4284f3eff0b65a4'),
	(13,'Katrina','Hernandez','littlewilliam@clark.com','6011047225447742','Katrina.Hernandez','e4e04e3c8fadbb1ce29fb5052fa6a46a'),
	(14,'David','Smith','jasminemills@burns.com','210025820289489','David.Smith','e61ddf40a398895d7854f3f89f3b48cf'),
	(15,'Cody','Wood','bmata@yahoo.com','4628754084771282','Cody.Wood','a7fa267041aa0790143a45bab0095431'),
	(16,'Heather','Brown','cbailey@gmail.com','4614724397207841','Heather.Brown','f12b42f2b5f7cd434bd855fb9dd7fe96'),
	(17,'Elizabeth','Harrison','albert98@moody-ruiz.com','348138904569893','Elizabeth.Harrison','8f4e2564b5eadfdf4e40b33fb76adc8f'),
	(18,'Edward','Smith','emmamurray@hotmail.com','4297638945640','Edward.Smith','3e5c32908e5743719d0f171d9be6787b'),
	(19,'David','Evans','bushpamela@haynes.info','3096154386133791','David.Evans','a1f0ea24a92bcfc160d037163a15fd06'),
	(20,'Craig','Campbell','justinferguson@martin.com','3112939890533894','Craig.Campbell','7b91021c1ff76e1828674337e35585aa'),
	(21,'Jose','Green','hamiltonstephen@wilcox-fitzpatrick.com','210025285399914','Jose.Green','9899edaba7894761f4e5c5a833652c56'),
	(22,'Andrew','Woods','jessica78@torres-smith.com','561270991160','Andrew.Woods','283751823244b1af369c7c46957d13c9'),
	(23,'Carl','Oconnor','ariaskenneth@turner.com','5302250024219938','Carl.Oconnor','1a88d8b8203a91710d34cc2015af5773'),
	(24,'Wesley','Romero','emily96@gmail.com','3337340451412215','Wesley.Romero','88a8a808596d2ac0a276b0c60c9dfdde'),
	(25,'Tonya','Burgess','robertgilbert@craig.org','180019801617913','Tonya.Burgess','0593b7b6be3bfb617cc892eb4661c529'),
	(26,'Jeremy','Patel','toni50@yahoo.com','4392429456160661','Jeremy.Patel','6353b343c8639fe5ffd2a78d853fe519'),
	(27,'Tyler','Kelly','zachary75@nelson-luna.com','210051586608068','Tyler.Kelly','e930c7d989d9e107fff7e238a18ae0ef'),
	(28,'Kyle','Allison','thomaspatrick@parker.org','3337327740095323','Kyle.Allison','50b06d83e300f9ac553606feaa25a25b'),
	(29,'Cameron','Marks','bennettmichael@yahoo.com','30049521091679','Cameron.Marks','63d4081d769a4aba7c3fbc398e1ca3b7'),
	(30,'Jason','Cox','amyreeves@yahoo.com','341706454551581','Jason.Cox','378134ac365769c3065824e8aec8870e'),
	(31,'Benjamin','Hayes','christopher56@hotmail.com','6011840109485600','Benjamin.Hayes','f67d1cdbe4ca0724c6e2b6807ac195a3'),
	(32,'Juan','Johnson','pbrooks@gmail.com','376361899690913','Juan.Johnson','53a0f35460bd622065905bee4af3927f'),
	(33,'Wendy','Blair','stephensjill@gmail.com','502045225963','Wendy.Blair','cc0a24b16e1c5fe6da23f1216973bcaa'),
	(34,'Deborah','Martinez','garciajonathan@yahoo.com','379801112854025','Deborah.Martinez','47430ecb477f78b5c676c41f0d1efd51'),
	(35,'Steven','Green','hamiltonleslie@hotmail.com','3158898981590541','Steven.Green','5dda4f3d692fe083bec43538134528ce'),
	(36,'Brittany','Foster','karen96@yahoo.com','210007743599640','Brittany.Foster','f055221e36018afe2028724549ad4074'),
	(37,'Donna','Brown','earl33@gmail.com','6011481950842795','Donna.Brown','21abc80cbcf52287d4eb2154641688cc'),
	(38,'Thomas','Webb','reyesdylan@clark.com','869904800815485','Thomas.Webb','d21ba727b8401c16ecbc031e01437fdf'),
	(39,'Krystal','Perez','karenlawson@sandoval-edwards.com','4752096329706568','Krystal.Perez','5a8354e326168460ac511ce7328b5dd6'),
	(40,'Angelica','Moss','gibsongregory@yahoo.com','6011160542606211','Angelica.Moss','0ca56938f71023a3ed4c79874fe4dcac'),
	(41,'Katelyn','Armstrong','jeffreyjohnson@thompson.info','6011669749984841','Katelyn.Armstrong','1677974c5bdc4539a86e838a62bf139e'),
	(42,'Matthew','Roman','josewu@melendez.com','3158619898763470','Matthew.Roman','0086403da96248446ad62745b2c0acf0'),
	(43,'Elizabeth','Reeves','torresdeborah@hotmail.com','639085092257','Elizabeth.Reeves','6e9c395778a23e16000a9512c448a446'),
	(44,'Timothy','Beck','jack34@jenkins.com','4701769148619475','Timothy.Beck','2b01008767cf7dab9e63b1629095ef56'),
	(45,'Michael','Robertson','davissteven@powell.com','30207784794047','Michael.Robertson','99916d78d1cc7287a05b3a4d7f3fae17'),
	(46,'Gabriela','Patel','steven02@yahoo.com','869994930479047','Gabriela.Patel','09055ee92b46de77d769885da8c9ca63'),
	(47,'Jeff','Hanson','markcameron@ward.org','501838490354','Jeff.Hanson','178e720dbc6bda00c69abc3a3db7099d'),
	(48,'Katherine','Garcia','christinasmith@gmail.com','210048350767120','Katherine.Garcia','bb18cffbe5b9e5142615f69c77a560db'),
	(49,'Michael','Cooper','amber44@rivera.biz','4128287977514043','Michael.Cooper','1c813da1c756d6433721e55ae7d0a068'),
	(50,'Veronica','Bradshaw','harrisonheather@dunn.com','6011733292581618','Veronica.Bradshaw','c00084f4eacbe145da409f01c87447a3'),
	(51,'Joseph','Collins','ksuarez@russell.com','30404406066132','Joseph.Collins','cdb3b645cd3fb9c49804f6a6a1661135'),
	(52,'Mercedes','Garcia','hillmary@lowe.info','3158582452061816','Mercedes.Garcia','bbbc3e15e820b8129ececae5e3480694'),
	(53,'Holly','Simpson','uchristensen@brown.com','6011271734098776','Holly.Simpson','77e8112263bb90d9d357389137a429ff'),
	(54,'Steven','Sheppard','angelapope@gmail.com','3112042253622642','Steven.Sheppard','c85384b42eaf416211ba099cfe05271c'),
	(55,'Ashley','Brandt','gregoryhart@yahoo.com','3088022965281499','Ashley.Brandt','6cfadde94cd166ddbbe2304a3ead7e9c'),
	(56,'Destiny','Diaz','fmorgan@hotmail.com','4762775038988','Destiny.Diaz','e46873a4b5285a6de4df55edb826f868'),
	(57,'Carlos','Salazar','ycarter@graham.com','630497084958','Carlos.Salazar','1fb08e3c6c5dcdc5a6d9222cf3508e6f'),
	(58,'Alex','Brown','marisabrown@yahoo.com','210091320841104','Alex.Brown','143d5943473a358f7e5eb7aabb88d5a3'),
	(59,'Eric','Blake','dixonjermaine@hotmail.com','6011262884534165','Eric.Blake','1773a08fbb3b8ea49b09943d4fca0d23'),
	(60,'Krista','Hanson','denise42@yahoo.com','4932063064807913','Krista.Hanson','c625d1ee93b40e68b064ad3278a79c97'),
	(61,'John','Johnson','rstanley@yahoo.com','6011468231776074','John.Johnson','12a33a5f953dc72ef6635ea23bf18c38'),
	(62,'Eric','Warren','sbates@nelson.net','347702985683948','Eric.Warren','8a0f55c4f7f87a3fc8355f78eef2b59e'),
	(63,'Todd','Ramirez','thomasmay@sullivan.com','5174569288948873','Todd.Ramirez','b9464e297c66b9e576ced6be16ee286f'),
	(64,'Kevin','Roberts','jalexander@gmail.com','3096019435442644','Kevin.Roberts','d083630bf2abb9df4bf3d91acabd3962'),
	(65,'Jessica','Stevenson','nalexander@gmail.com','6011079444732869','Jessica.Stevenson','a576c9d8e2b6d4102f0f1912920cbe7d'),
	(66,'Kimberly','Williams','dorothy50@hotmail.com','5367532341504502','Kimberly.Williams','c72816a70a47411cb5a2c6e023f38ebf'),
	(67,'Heather','Chan','victor46@jones.biz','5391679967229615','Heather.Chan','46208b7f9957e9ba94f5ef08326fdc0d'),
	(68,'Rachel','Bennett','zrichmond@ponce.org','3088542468014733','Rachel.Bennett','f31e2de18597bc9d9fd9faf8a9f92020'),
	(69,'Katherine','Johnson','sean05@hotmail.com','210084185357027','Katherine.Johnson','1d847a6f4b50d68dee84d48e59f8b6ff'),
	(70,'Ashlee','Harris','herreratracy@rodriguez-walsh.biz','30094764216252','Ashlee.Harris','07cca911eed8d15576359892e79691e1'),
	(71,'Kimberly','Thompson','gregoryhamilton@yahoo.com','343636005826687','Kimberly.Thompson','c985e6c465867efdb1b11b19e2e36238'),
	(72,'Stephanie','Pace','april23@gmail.com','869984787931190','Stephanie.Pace','db6a46a8bcc464d2348c5222f0a40e20'),
	(73,'Cody','Martinez','colleen51@gmail.com','5542904280261097','Cody.Martinez','e2a3ece7d8578f62bc7a9c33282159be'),
	(74,'Scott','Nguyen','bthompson@hotmail.com','3528111773320439','Scott.Nguyen','420592f0b76c11ed65eebf07d7e0d0b9'),
	(75,'Tracy','Weaver','gcamacho@yahoo.com','6011755776342035','Tracy.Weaver','6530ad6c9912cebfdd3b442eefcca392'),
	(76,'Cheryl','Ochoa','johnphillips@yahoo.com','4390676432157','Cheryl.Ochoa','e961e8aee674ab91b09962b855e818c9'),
	(77,'Deborah','Byrd','wcortez@walker.com','676256640720','Deborah.Byrd','87300215216f0b96832253c139c03f2f'),
	(78,'Cristina','Brown','fbrown@jones-rose.net','4541400925789','Cristina.Brown','b15829a33fbfe66e59a8f5cd29cb540a'),
	(79,'Margaret','Watson','melissa43@vazquez.com','4977399944406286','Margaret.Watson','54e5caa9139563a73f1c1eced65a7e98'),
	(80,'Zachary','Thomas','xmitchell@gmail.com','676201763585','Zachary.Thomas','6d1ec3b72198b89ce9c39dca33f097bc'),
	(81,'Nathaniel','Steele','robert23@gmail.com','30428805836175','Nathaniel.Steele','6ddd591a8485df97610d15900309a227'),
	(82,'Robert','Edwards','hbutler@hotmail.com','4147847431423433','Robert.Edwards','7b906fe391a763092737d7d640cc9cc3'),
	(83,'Melissa','Maxwell','lisawest@martinez.com','30487508937656','Melissa.Maxwell','b4d119741b3782cbab98ef6f55ff613e'),
	(84,'Peter','Carson','blozano@gmail.com','4256446190849','Peter.Carson','4b666efb3f00ac2dfe2015eb54586eca'),
	(85,'Sarah','Lewis','carl27@yahoo.com','4155797938480296','Sarah.Lewis','3aaf961628e94394eef89fdbcbd00cad'),
	(86,'Carol','Marquez','jamescarter@rollins-schmidt.com','3112502612084792','Carol.Marquez','e549d3347654001fbc91fe5258426376'),
	(87,'Jose','Mendez','hthomas@andrews-wu.com','210028617577231','Jose.Mendez','c915c048942982cf989a855720e91d5d'),
	(88,'Deanna','White','hwallace@lee-griffin.com','30237795888868','Deanna.White','1143ede05377a0cc7cb66663a2e1dc5d'),
	(89,'Michael','Ramos','ariel85@gmail.com','4651536197507525','Michael.Ramos','e13bbec655ff24b4669c68e889da4a3c'),
	(90,'Kimberly','Decker','dale73@hotmail.com','4964782186715','Kimberly.Decker','a58684116242dfe31f44cf0214699afe'),
	(91,'Joseph','Hale','watkinsjacob@randolph.org','3088493822482721','Joseph.Hale','76896ad7944bdff36e6eaab34aa5627e'),
	(92,'Nicole','Spencer','lewisjohn@yahoo.com','6011349789351686','Nicole.Spencer','e14e35fea023e365004f7b4295a56493'),
	(93,'John','Young','barbara65@hotmail.com','3158817061340791','John.Young','95d939a3b1f8bfc2d483bc26ff985010'),
	(94,'Jon','Wells','brianbaxter@hotmail.com','4515901549603','Jon.Wells','202df954fa1bc5055865ff3108bed1a9'),
	(95,'Carmen','Munoz','jaime23@hotmail.com','3158646249716790','Carmen.Munoz','7ada504a6d4fa6a1b297b562f0d21796'),
	(96,'Brandon','Robinson','sgonzales@perry-davila.info','3096769681367966','Brandon.Robinson','5124b14a0a928a0087aeb0cfe4024098'),
	(97,'Faith','Smith','kturner@yahoo.com','4003743097753192','Faith.Smith','1afd9abf0e4bd2f9fd9321fdb981dfb7'),
	(98,'Joseph','Leblanc','robert66@glover.net','30361949374326','Joseph.Leblanc','631c86ceeb0f7884c991352539f9252b'),
	(99,'Alice','Young','kathryn19@delgado.com','4566827157141080','Alice.Young','0781069ab70c20c7a17f075b01518449'),
	(100,'Amanda','Horn','theresa74@hotmail.com','3112003281974967','Amanda.Horn','c21d3913893957a5a9eb7ff4855ea541');

/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(80) DEFAULT NULL,
  `password` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `username`, `password`)
VALUES
	(1,'admin','0192023a7bbd73250516f069df18b500'),
	(2,'bruce.banner','9ddbc04c5ac2ca10ce94f59e270a7212'),
	(3,'steve.rogers','24d45a21e53ef853b56620fa189c0d43'),
	(4,'tony.stark','0d94d92e3dc096f64213a5b34fa9d098');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
